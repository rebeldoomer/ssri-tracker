import csv
from datetime import date
from collections import defaultdict
import calendar
from reportlab.pdfgen import canvas
from reportlab.lib import styles
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Table, TableStyle
from reportlab.lib.pagesizes import letter
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet

# Define global variables
ssri_file = 'current_ssri.txt'
data_file= 'medication_log.csv'

def export_to_pdf(data, filename='medication_log.pdf'):
    doc = SimpleDocTemplate(filename, pagesize=letter)
    elements = []

    style = getSampleStyleSheet()
    title = Paragraph('Medication Log', style['Title'])
    elements.append(title)
    elements.append(Spacer(1, 12))

    # Update data to include 'mg' and labels for scores
    updated_data = []
    for row in data:
        updated_row = row[:]
        updated_row[1] += 'mg'  # Add 'mg' to the dose
        updated_row[2] = f"Anxiety: {updated_row[2]}"  # Label for anxiety score
        updated_row[3] = f"Depression: {updated_row[3]}"  # Label for depression score
        updated_data.append(updated_row)

    # Create table with updated data
    table = Table(updated_data)
    table_style = TableStyle([
        ('BACKGROUND', (0, 0), (-1, 0), colors.grey),
        ('TEXTCOLOR', (0, 0), (-1, 0), colors.whitesmoke),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
        ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
        ('BOTTOMPADDING', (0, 0), (-1, 0), 12),
        ('BACKGROUND', (0, 1), (-1, -1), colors.beige),
        # Additional styles if necessary
    ])
    table.setStyle(table_style)

    elements.append(table)
    doc.build(elements)


def get_last_dose():
    try:
        with open(data_file, 'r', newline='') as file:
            rows = list(csv.reader(file))
            if rows:
                return rows[-1][1]
            return None
    except FileNotFoundError:
        return None

def add_entry(dose, anxiety_rating, depression_rating, ssri_symptoms):
    if not dose:
        dose = get_last_dose() or "Not specified"

    with open(data_file, 'a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow([date.today(), dose, anxiety_rating, depression_rating, ssri_symptoms])

def get_or_set_ssri(first_run=False):
    try:
        with open(ssri_file, 'r') as file:
            ssri = file.read().strip()
            if first_run:
                print(f"Current SSRI: {ssri}")
            return ssri
    except FileNotFoundError:
        if first_run:
            set_ssri()

def set_ssri():
    ssri = input("Which SSRI are you on? ")
    with open(ssri_file, 'w') as file:
        file.write(ssri)
    print(f"SSRI set to: {ssri}")

def read_entries():
    try:
        with open(data_file, 'r', newline='') as file:
            for row in csv.reader(file):
                ssri_symptoms = row[4] if len(row) > 4 else "Symptoms not provided"
                print(f"Date: {row[0]}, Dose: {row[1]}mg, Anxiety: {row[2]}, Depression: {row[3]}, SSRI Symptoms: {ssri_symptoms}")
    except FileNotFoundError:
        print("No entries found. Start by adding a new entry.")

def calculate_and_display_averages():
    scores = defaultdict(lambda: {'anxiety': [], 'depression': []})
    try:
        with open(data_file, 'r', newline='') as file:
            reader = csv.reader(file)
            for row in reader:
                entry_date = date.fromisoformat(row[0])
                month_year = entry_date.strftime("%Y-%m")
                scores[month_year]['anxiety'].append(int(row[2]))
                scores[month_year]['depression'].append(int(row[3]))

        for month_year, data in scores.items():
            anxiety_avg = sum(data['anxiety']) / len(data['anxiety'])
            depression_avg = sum(data['depression']) / len(data['depression'])
            print(f"\n{calendar.month_name[int(month_year.split('-')[1])]} {month_year.split('-')[0]} Averages:")
            print(f"Anxiety: {anxiety_avg:.2f}, Depression: {depression_avg:.2f}")
            for condition, avg_score in [('Anxiety', anxiety_avg), ('Depression', depression_avg)]:
                print(f"{condition} Summary: {score_comment(avg_score)}")

    except FileNotFoundError:
        print("No entries found. Start by adding a new entry.")


def score_comment(score):
    if score <= 1:
        return "Balanced mood."
    elif 1 < score <= 3:
        return "Slightly anxious/depressed."
    elif 3 < score <= 5:
        return "Moderately anxious/depressed."
    elif 5 < score <= 7:
        return "Quite anxious/depressed."
    elif 7 < score <= 9:
        return "Highly anxious/depressed."
    else:
        return "Extremely anxious/depressed."

def main():
    get_or_set_ssri(first_run=True)

    while True:
        print("\n1. Add new entry")
        print("2. Read entries")
        print("3. Display monthly averages")
        print("4. Change Which SSRI You're Taking")
        print("5. Export to PDF")
        print("q. Exit -_-")
        choice = input("Select an option: ")

        if choice == '1':
            last_dose = get_last_dose()
            if last_dose:
                change_dose = input(f"Last dose was {last_dose}mg. Change dose? (y/N): ")
                dose = input("Enter new Zoloft dose (mg): ") if change_dose.lower() == 'y' else last_dose
            else:
                dose = input("Enter today's Zoloft dose (mg): ")

            anxiety_rating = input("Today's anxiety rating (0-10): ")
            depression_rating = input("Today's depression rating (0-10): ")
            ssri_symptoms = input("Describe any symptoms you're experiencing today: ")
            add_entry(dose, anxiety_rating, depression_rating, ssri_symptoms)
        elif choice == '2':
            read_entries()
        elif choice == '3':
            calculate_and_display_averages()
        elif choice == '4':
            set_ssri()
        elif choice == '5':
            # Assuming we're gathering data for the PDF export here
            try:
                with open(data_file, 'r', newline='') as file:
                    data = list(csv.reader(file))
                    export_to_pdf(data)  # This calls the top-level function
                    print("Exported to PDF successfully.")
            except FileNotFoundError:
                print("No entries found to export.")
        elif choice.lower() == 'q':
            print("Exiting program.")
            break
        else:
            print("Invalid selection. Please try again.")

# Call the main function when the script is run
if __name__ == "__main__":
    main()


# todo .pdf export
# todo graphics
# todo medication reminder
# todo email monthly summary
# todo symptom correlations
# todo interactive dashboard
# todo progress tracker
# todo Data Visualization: heatmaps or mood trend lines over time using matplotlib or seaborn.
