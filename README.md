## SSRI Tracker
### Overview

The SSRI Tracker is a personal project designed to track and analyze the effects of medications, tracking mood and symptoms over time. This tool aims to offer users a view of their treatment progress, providing insights into the effectiveness of their medication regimen. It is intended as a supplementary tool to aid in personal health management and should not substitute for professional medical advice.

### Features

- Symptom and Mood Logging: Easily record daily medication doses along with anxiety and depression severity scores.

- Monthly Summary: Get a consolidated view of your monthly progress with average scores to gauge the effectiveness of your treatment.

- Data-Driven Insights: Through future updates, the tracker will offer graphical analysis, trend insights, and correlation analysis between dosage adjustments and symptom changes.


### Getting Started
Usage Instructions

- Add New Entry: Log today's medication dose along with your anxiety and depression ratings.
- Read Entries: Review your historical entries to monitor progress.
- Display Monthly Averages: View your average mood and symptom scores for each month.
- Change Which SSRI You're Taking: Update your current medication as needed.
- Exit: Safely close the application.

#### Planned features

- [ ] Graphical Representations: Incorporate visualizations to make tracking trends more intuitive.
- [ ] Trend Analysis: Develop algorithms to identify patterns or triggers in symptom fluctuations.
- [ ] Correlation Analysis: Assess the impact of dosage adjustments on symptom improvement.
- [ ] Export Options: Implement support for exporting data in various formats for easier sharing with healthcare professionals.


#### Contributing

Your contributions can help make the SSRI Tracker more robust and beneficial for everyone. Whether it's suggesting new features, improving existing functionality, or fixing bugs, your input is welcome.


#### Project Status

Still a big mess, needs to heavily get rewritten.